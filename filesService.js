const fs = require('fs');
const fsp = require('fs/promises');
const path = require('path');
const filesFolder = path.resolve('./files');

const db = { 'notes34.txt': '123qwe' }

const checkPass = (fileName, pass) =>{
  if (fileName in db){
    if (db[fileName] === pass){
      return 'passok'
    }
    else {
      return 'passerr';
    }
  }
  else {
    return 'nodbrec';
  }
}

function checkExt(fileName){
  const extArr = ['.log', '.txt', '.json', '.yaml', '.xml', '.js']
  const ext = path.extname(fileName);
  console.log('ext', ext, extArr.includes(ext))
  return extArr.includes(ext);
}

function createFile (req, res, next) {
  try {
    const { filename, content } = req.body;
    if (filename === undefined) {
      res.status(400).send({ message: "Please specify 'filename' parameter" });
      return;
    }

    if (content === undefined) {
      res.status(400).send({ message: "Please specify 'content' parameter" });
      return;
    }

    if(!checkExt(filename)){
      res.status(400).send({
        message: `File extention should be one of the followings: '.log', '.txt', '.json', '.yaml', '.xml', '.js'`,
      });
      return;
    }
      
    fs.writeFile(`${filesFolder}/${filename}`, content, (err) => {
        if (err) {
          res.status(500).send({ message: 'Server error' });
          return;
        }
      });

      db[req.body.filename] = req.query.password;
      res.status(200).send({ message: 'File created successfully' })

  } catch (error) {
    res.status(500).send({ message: 'Server error' });
  }
}


function getFiles (req, res, next) {
  const filesList = [];
  try {
    fs.readdir(filesFolder, (err, result) => {
      if (err) {
        res.status(400).json({message: 'Client error'});
      }
      result.forEach(file => {
        filesList.push(file);
      })
      res.status(200).send({
        "message": "Success",
        "files": filesList});
    })
  } catch (error) {
    res.status(500).json({message: 'Server error' });
  }
}

const getFile = async (req, res, next) => {
  try {
    const fileName = req.params.filename;
    const pass = req.query.password;

    const passCheck = checkPass(fileName, pass)

    if (passCheck === 'passerr'){
      res.status(400).send({ "message": "Server error - wrong password" });
      return;
    }
    if (passCheck === 'nodbrec' || passCheck === 'passok'){
      readFile(fileName, res, next)
    }
  }
  catch(err){
    throw err;
  }
}



  const readFile = async (fileName, res, next) =>{

    try {
    let uploadedDate;
    let extension = path.extname(fileName).split('.')[1];

    fs.stat(filesFolder + "\/" + fileName, (error, stats) => {
      // console.log('fs.stat req.url', req.url)
      if (error) {
        console.log(error);
        return;
      }
      uploadedDate = stats.birthtime;
    });
    res.status(200).send({
      "message": "Success",
      "filename": path.basename("\/" + fileName),
      "content": await fsp.readFile(filesFolder + "\/" +fileName, { encoding: 'utf8' }),
      "extension": extension,
      "uploadedDate": uploadedDate});
  } catch (error) {
    res.status(400).json({message: 'Server error ' + error});
  }
}

function updateFile (req, res, next) {
  try {
    const fileName = req.params.filename;
    const pass = req.query.password;

    const passCheck = checkPass(fileName, pass)

    if (passCheck === 'passerr'){
      res.status(400).send({ "message": "Server error - wrong password" });
      return;
    }
    if (passCheck === 'nodbrec' || passCheck === 'passok'){

    const { filename, content } = req.body;
    if (filename === undefined) {
      res.status(400).send({ message: "Please specify 'filename' parameter" });
      return;
    }

    if (content === undefined) {
      res.status(400).send({ message: "Please specify 'content' parameter" });
      return;
    }

    fs.stat(path.join(filesFolder, filename), (err, stats) => {
      if (err) {
        res.status(400).send({ message: `No file with '${filename}' filename found` })
        return
      }

      fs.writeFile(path.join(filesFolder, filename), content, (err) => {
        if (err) {
          res.status(500).send({ message: 'Server error' })
          return
        }

        res.status(200).send({ message: `${filename} is successfully updated` })
      })
    })
   }
  } catch (error) {
    res.status(500).send({ message: 'Server error' })
  }
}

function deleteFile (req, res, next) {
  try {
    const fileName = req.params.filename;
    const pass = req.query.password;

    const passCheck = checkPass(fileName, pass)

    if (passCheck === 'passerr'){
      res.status(400).send({ "message": "Server error - wrong password" });
      return;
    }
    if (passCheck === 'nodbrec' || passCheck === 'passok'){

    const { filename } = req.params;

    fs.unlink(path.join(filesFolder, filename), (err) => {
      if (err) {
        res.status(400).send({ message: `No file with '${filename}' filename found` });
        return;
      }

      res.status(200).send(`${filename} is successfully deleted`);
    })
   }
  } catch (error) {
    res.status(500).send({ message: 'Server error' });
  }
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  updateFile,
  deleteFile
}
